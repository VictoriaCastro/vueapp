const app = new Vue ({

	el: "#app",
	data: {

		cantidad: 6,
		clientes: [],
		aceptados: 0,
		rechazados:0,
		genero: true,
    cuidad: true,
		nombre:true,
		apellido:true,
	},

	methods:{

		obtenerClientes: function(){
			axios
			.get('https://randomuser.me/api/?results=' + this.cantidad)
			.then(response =>{
				this.clientes = response.data.results;
			})
		},

		aceptarCliente: function(posicion){
			this.aceptados++;
			this.clientes.splice(posicion, 1);

		}, // cierra la funcion aceptarCliente
		rechazarCliente: function(posicion){
			this.rechazados++;
			this.clientes.splice(posicion, 1);

		}, // cierra la funcion rechazarCliente

		filInfo: function () {
            (document.querySelector('#genero').checked) ? this.genero = true: this.genero = false;
            (document.querySelector('#ciudad').checked) ? this.ciudad = true: this.ciudad = false;
            (document.querySelector('#apellido').checked) ? this.apellido = true: this.apellido = false;
						(document.querySelector('#nombre').checked) ? this.nombre = true: this.nombre = false;

        },

		} // llave que cierra methods
  }) // cierra el Vue
